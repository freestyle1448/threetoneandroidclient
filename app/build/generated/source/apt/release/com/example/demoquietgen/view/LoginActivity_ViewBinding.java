// Generated code from Butter Knife. Do not modify!
package com.example.demoquietgen.view;

import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.demoquietgen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding implements Unbinder {
  private LoginActivity target;

  private View view7f080096;

  private View view7f080080;

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActivity_ViewBinding(final LoginActivity target, View source) {
    this.target = target;

    View view;
    target.loginET = Utils.findRequiredViewAsType(source, R.id.loginEditText, "field 'loginET'", EditText.class);
    view = Utils.findRequiredView(source, R.id.passwordEditText, "field 'passwordET' and method 'submitLogin'");
    target.passwordET = Utils.castView(view, R.id.passwordEditText, "field 'passwordET'", EditText.class);
    view7f080096 = view;
    ((TextView) view).setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView p0, int p1, KeyEvent p2) {
        return target.submitLogin(p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.loginButton, "field 'loginButton' and method 'onLogin'");
    target.loginButton = Utils.castView(view, R.id.loginButton, "field 'loginButton'", Button.class);
    view7f080080 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onLogin();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.loginET = null;
    target.passwordET = null;
    target.loginButton = null;

    ((TextView) view7f080096).setOnEditorActionListener(null);
    view7f080096 = null;
    view7f080080.setOnClickListener(null);
    view7f080080 = null;
  }
}

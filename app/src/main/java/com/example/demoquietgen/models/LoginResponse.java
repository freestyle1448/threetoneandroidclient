package com.example.demoquietgen.models;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public
class LoginResponse {
    @SerializedName("err")
    private ErrorResponse err;
    private User user;
    @SerializedName("token")
    private String token;
}

package com.example.demoquietgen.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public
class Card {
    private String name;
    private Attributes attributes;

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public class Attributes {
        private String CardName;
        private String CardId;
        private String CardHolder;
        private String Status;
        private Boolean NoCVV;
        private Boolean Expired;
        private Integer ExpMonth;
        private Integer ExpYear;
    }
}

package com.example.demoquietgen.models;

public final class Constants {
    public static final String APP_PREFERENCES = "userdata";
    public static final String USER_LOGIN = "userLogin";
    public static final String USER_TOKEN = "userToken";
}

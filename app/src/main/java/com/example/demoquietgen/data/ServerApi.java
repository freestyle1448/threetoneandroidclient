package com.example.demoquietgen.data;

import com.example.demoquietgen.models.LoginRequest;
import com.example.demoquietgen.models.LoginResponse;
import com.example.demoquietgen.models.User;
import com.example.demoquietgen.models.UserInfoResponse;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ServerApi {

    @POST("/api/user/auth")
    Call<LoginResponse> login( @Body LoginRequest loginRequest);

    @GET("api/user/card/list")
    Call<JsonObject> getCards(@Header("Token") String userToken);

    @GET("api/user/info")
    Call<UserInfoResponse> getUserInfo(@Header("Token") String userToken);
}

package com.example.demoquietgen.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.demoquietgen.LoyaltyCardReader;
import com.example.demoquietgen.R;
import com.example.demoquietgen.data.ServerApi;
import com.example.demoquietgen.data.ServiceGenerator;
import com.example.demoquietgen.models.Card;
import com.example.demoquietgen.models.User;
import com.example.demoquietgen.models.UserInfoResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ncorti.slidetoact.SlideToActView;

import org.quietmodem.Quiet.FrameTransmitter;
import org.quietmodem.Quiet.FrameTransmitterConfig;
import org.quietmodem.Quiet.ModemException;

import java.io.IOException;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.demoquietgen.models.Constants.APP_PREFERENCES;
import static com.example.demoquietgen.models.Constants.USER_TOKEN;


public class TransmitActivity extends AppCompatActivity implements LoyaltyCardReader.AccountCallback {
    public static final String TAG = "CardReaderFragment";
    // Recommend NfcAdapter flags for reading from other Android devices. Indicates that this
    // activity is interested in NFC-A devices (including other Android devices), and that the
    // system should not check for the presence of NDEF-formatted data (e.g. Android Beam).
    public static int READER_FLAGS =
            NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK;
    public LoyaltyCardReader mLoyaltyCardReader;
    private final Gson gson = new Gson();
    private final ServerApi serverApi = ServiceGenerator.createService(ServerApi.class);
    private FrameTransmitter transmitter;
    private String userToken;
    private TextView cardName;
    private ImageView cardIcon;
    private SlideToActView slideToActView;

    private Card card;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transmit);
        getSupportActionBar().setElevation(0);
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int media_max_volume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        audioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC, // Stream type
                media_max_volume, // Index
                AudioManager.FLAG_SHOW_UI // Flags
        );
        SharedPreferences sharedPreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        userToken = sharedPreferences.getString(USER_TOKEN, "");

        findViewById(R.id.exitButton).setOnClickListener(v -> {
            SharedPreferences preferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.apply();

            Intent intent = new Intent(TransmitActivity.this, LoginActivity.class);
            //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });
        //cardName = findViewById(R.id.cardName);
        //cardIcon = findViewById(R.id.cardView);

        slideToActView = findViewById(R.id.welcome_slider);
        slideToActView.setOnSlideCompleteListener(v -> {
            handleSendClick();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            slideToActView.resetSlider();
        });
        slideToActView.setText("");

        mLoyaltyCardReader = new LoyaltyCardReader(this);
        enableReaderMode();
        setupTransmitter();
        handleDataFromIntent();
        slideToActView.setVisibility(View.VISIBLE);
        //initCards();
        getUserInfo();

    }


    @Override
    public void onPause() {
        super.onPause();
        disableReaderMode();
    }

    @Override
    public void onResume() {
        super.onResume();
        enableReaderMode();
    }

    private void enableReaderMode() {
        Log.i(TAG, "Enabling reader mode");
        Activity activity = this;
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(activity);
        if (nfc != null) {
            nfc.enableReaderMode(activity, mLoyaltyCardReader, READER_FLAGS, null);
        }
    }

    private void disableReaderMode() {
        Log.i(TAG, "Disabling reader mode");
        Activity activity = this;
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(activity);
        if (nfc != null) {
            nfc.disableReaderMode(activity);
        }
    }
    private void getUserInfo() {
        serverApi.getUserInfo(userToken).enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                if (response.body() != null)
                    user = response.body().getUser();
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {

            }
        });
    }

    private void initCards() {
        serverApi.getCards(userToken).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    card = gson.fromJson(response.body().get("response")
                            .getAsJsonObject()
                            .get("root")
                            .getAsJsonObject()
                            .getAsJsonArray("children").get(0).getAsJsonObject().toString(), Card.class);
                    if (card != null) {
                        if (!card.getAttributes().getExpired() && card.getAttributes().getStatus().equals("IsActive")) {
                            String cardNameId = card.getAttributes().getCardName();
                            String cardBank = "SBER";

                            TransmitActivity.this.runOnUiThread(() -> {
                                Drawable bankIcon = null;
                                switch (cardBank) {
                                    case "SBER":
                                        bankIcon = getDrawable(R.drawable.icon200);
                                        break;
                                }

                                cardName.setText(
                                        String.format(Locale.getDefault(), "%s %s",
                                                "VISA", cardNameId.substring(cardNameId.length() - 4)));
                                cardIcon.setImageDrawable(bankIcon);
                                slideToActView.setVisibility(View.VISIBLE);
                            });
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                System.err.println(t.getLocalizedMessage());
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (transmitter != null) {
            transmitter.close();
        }
    }


    private void setupTransmitter() {
        FrameTransmitterConfig transmitterConfig;
        try {
            transmitterConfig = new FrameTransmitterConfig(
                    this, "ultrasonic-experimental");
            transmitter = new FrameTransmitter(transmitterConfig);
        } catch (IOException | ModemException e) {
            throw new RuntimeException(e);
        }
    }

    private void handleSendClick() {
        if (transmitter == null) {
            setupTransmitter();
        }
        if (user != null)
            send();
        else {
            Toast.makeText(this, "Нет информации о пользователе! Попробуйте выполнить вход ещё раз!", Toast.LENGTH_LONG).show();
        }
    }

    private void send() {
        try {
            final String formatString = String.format("|%s|", user.getPin());
            for (int i = 0; i < 4; i++) {
                transmitter.send(formatString.getBytes());
                Thread.sleep(1200);
            }

        } catch (IOException e) {
            // our message might be too long or the transmit queue full
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void handleDataFromIntent() {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);

            }
        }
    }

    @Override
    public void onAccountReceived(String account) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String url = account;
                Intent browserIntent =
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });
    }
}
